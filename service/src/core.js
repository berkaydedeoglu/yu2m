/**
 * --- core.js ---
 *  
 * The core module of YU2M service.
 * This module consists of core functions which are using
 * for converting.
 *
 * Date: 01.04.2019
 * Time: 18.52 
 * Author: Berkay Dedeoglu
 * E-mail: berkay.dedeoglu@outlook.com
 **/

module.exports = {
    "converter" : {
	"unicode2morse" : unicode2morse,
	"morse2unicode" : morse2unicode
    }
};

/**
 * This function converts a unicode word to morse code word.
 *
 * "Word" does not means normal word that English has. It can consist
 * white characters. 
 * 
 * If this function cannot convert a special character to morse, 
 * this will ignore the character. Also if the word has 2 or more 
 * white characters alongside, the function ignores unnecesaries.
 *
 * @param (string) word
 *     A string which is going to convert to morse code.
 *
 * @return
 *     Converted morse code.
 **/
function unicode2morse(word){
    var parsed = parse_word(word, " ");

    var words = parsed.map(word => convert(word, unicode_dictionary, " "));
    
    return words.join(" / ");
}


/**
 * This function converts morse code to human readable unicode 
 * string.
 * 
 * This uses morse_dictionary while converting. So if this cannot find
 * a key in dictionary this ignores the key.
 * 
 * Also if this function finds 2 or more white spaces together, this will
 * ignore unnecessaries also.
 *
 * @param (string) word
 *     A string which is going to convert to human readable unicode string.
 * 
 * @return
 *     Converted unicode string. 
 **/
function morse2unicode(word){

    var parsed = parse_word(word, delimiter='/');

    var converted = parsed.map(function (word){
	let splitted = word.split(' ');
	return convert(splitted, morse_dictionary, '');
    });
    
    
    return converted.join(' ');
}


/**
 * TODO: Yorum  ekle
 **/
function convert(word, dict, delimiter){
    // OPTIMIZE : Esneklik icin hizdan odun veriliyor olabilir.
    
    var converted = [];

    let limit = word.length;
    for (var i=0; i<limit; i++){
	var character = dict[word[i]];

	if (character)
	    converted.push(character);
	
    }

    return converted.join(delimiter);
}


/**
 * This function checks whether the word, which passed via as parameter,
 * is compatible with main functions. 
 * 
 * This function runs before morse2unicode or unicode2morse.
 * 
 * @param (string) word
 *     A string which is going to be normalized for main functions.
 * 
 * @param (string) delimiter 
 *     Main word delimiter (e.g <space>, |, \t)
 *
 *
 * @returns (string) 
 *    Normalized and parsed string array.
 **/
function parse_word(word, delimiter=" "){
    // OPTIMIZE: normalized'in olusturulmasinda yeni dizi kurulmasi buyuk bir sorun olabilir.
    // REVIEW: delimitter'a gercekten ihtiyac var mi?

    var lower_word = word.toLowerCase();

    // For defensive programming    
    // Splitted word for delimiters.
    var splitted = lower_word.split(delimiter);

    // Removing empty string items. Bcoz the split function
    // has a side effect it returns "" for " ". 
    splitted = splitted.filter(item => item != "");
    
    // Stripping white characters.
    // Words are preparing for main functions.
    var normalized = splitted.map(miniWord => miniWord.trim());
    
    return normalized;
}


// TODO: Sozlukler gelistirilmeli.
const unicode_dictionary = {
    'a' : '.-',    'b' : '-...',  'c' : '-.-.',
    'd' : '-..',   'e' : '.',     'f' : '..-.',
    'g' : '--.',   'h' : '....',  'i' : '..',
    'j' : '.---',  'k' : '-.-',   'l' : '.-..',
    'm' : '--',    'n' : '-.',    'o' : '---',
    'u' : '..-',   'p' : '.--.',  'q' : '--.-',
    'r' : '.-.',   's' : '...',   't' : '-',
    'y' : '-.--',  'v' : '...-',  'w' : '.--',
    'x' : '-..-',  'y' : '-.--',  'z' : '--..',
    '0' : '-----', '1' : '.----', '2' : '..---',
    '3' : '...--', '4' : '....-', '5' : '.....',
    '6' : '-....', '7' : '--...', '8' : '---..',
    '9' : '----.', '.' : '.-.-.-'
};

const morse_dictionary = {
    ".-" :   "a", "-..." :  "b", "-.-." : "c",
    "-.." :  "d", "." :     "e", "..-." : "f",
    "--." :  "g", "....":   "h", ".." :   "i",
    ".---":  "j", "-.-" :   "k", ".-..":  "l",
    "--" :   "m", "-." :    "n", "---" :  "o",
    ".--.":  "p", "--.-" :  "q", ".-." :  "r",
    "..." :  "s", "-" :     "t", "..-" :  "u",
    "...-":  "v", ".--":    "w", "-..-" : "x",
    "-.--":  "y", "--.." :  "z", "|" :    " ",
    "-----": "0", ".----":  "1", "..---": "2",
    "...--": "3", "....-":  "4", ".....": "5",
    "-....": "6", "--...":  "7", "---..": "8",
    "----.": "9", ".-.-.-": "."
};
