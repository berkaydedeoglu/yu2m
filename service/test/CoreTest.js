/**
 * Tests for Core.
 * Author: Berkay Dedeoglu
 * Date : 01.04.2019
 * Time : 21.40
 **/


var core = require('../src/core');
var assert = require('chai').assert;


var u2m = core.converter.unicode2morse;
var m2u = core.converter.morse2unicode;

describe('Core Conversion Tests', function () {
    it('Unicode to morse', function(){
	var result = u2m('Berkay Dedeoglu Kartaca');
	assert.equal(result, '-... . .-. -.- .- -.-- / ' +
		     '-.. . -.. . --- --. .-.. ..- / ' +
		     '-.- .- .-. - .- -.-. .-');
    });

    it('Numbers to morse', function(){
	var result = u2m('15460');
	assert.equal(result, ".---- ..... ....- -.... -----");
    });

    it("Numbers to morse 2", function(){
	var result = u2m('14.23');
	assert.equal(result, ".---- ....- .-.-.- ..--- ...--");
    });

    it('Empty string to morse',  function(){
	var result = u2m('');
	assert.equal(result, '');
    });

    it("Just delimiter to morse", function(){
	var result = u2m(" ");
	assert.equal(result, '');
    });

    it("Uppercase to morse" , function(){
	var lowercase = u2m("berkay");
	var uppercase = u2m("BerKaY");
	assert.equal(lowercase, uppercase);
    });

    it("Unknown characters to morse", function() {
	var withUnknown = u2m("simple word");
	var withoutUnkown = u2m("s#impl$e wo^rd");
	assert.equal(withUnknown. withoutUnknown);
    });

    it("Unnecessary white spaces", function() {
	var param1 = u2m("simple  word0");
	var param2 = u2m(" simple    word0 ");
	assert.equal(param1, param2);
    });

    it("Morse to unicode", function(){
	var result = m2u(".... . .-.. .-.. --- / .-- --- .-. .-.. -..");
	assert.equal(result, "hello world");
    });

    it("Numbers to unicode", function(){
	var result = m2u(".---- ----- -.... ...-- .-.-.- ..--- .....");
	assert.equal(result, "1063.25");
    });

    it("Empty string to unicode", function() {
	var result = m2u('');
	assert.equal(result, "");
    });

    it("Unknown characters to unicode", function() {
	var param1 = m2u(".---- . ----. . / ..--- ------ ....- ..-. .-.");
	var param2 = m2u(".---- . ----. . / ..--- ....- ..-. .-.");
	assert.equal(param1, param2);
    });

    it("Unnecessary white spaces", function() {
	var param1 = m2u("-... ..- .-. .---- -.-. --- -. / ....- ..... -- .- -.");
	var param2 = m2u('-... \n  ..- .-.   .---- -.-.  --- -.  /    ....- .....   --   .- -.');
	assert.equal(param1, param2);
    });
});


describe("Core String Manupulation Tests", function() {

    
    
});
